/*
# You can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, version 2.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatas_teorias_8047;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 *
 * @author M4RS
 */
public class Automatas_Teorias_8047_4 {
   
   private String input;

   public Automatas_Teorias_8047_4(String input) {

        this.input = input;
   }
   
   public boolean validation() {
        int size = input.length();
        int index = 0;
        boolean accepted_state = true;
        
        if(index < size && input.charAt(index) == 'b'){//q0
            index += 1;
            while (accepted_state && index < size && (input.charAt(index)== 'b')){ //q1
               index += 1;
            }
            if(index < size && input.charAt(index) == 'a'){//q2
               index += 1;
               accepted_state = true;
               if(index < size && input.charAt(index) == 'a'){//q3
                  index += 1;
                  accepted_state = true;
                  while (accepted_state && index < size && (input.charAt(index)== 'b' || input.charAt(index)== 'a')){ //q4
                     index += 1;
                  }
                  if(index < size && input.charAt(index) == 'b'){//q2
                     accepted_state = true;
                  }
               }else if(index < size && input.charAt(index) == 'b'){//q2
                     accepted_state = true;
               }
               while (accepted_state && index < size && (input.charAt(index)== 'b' || input.charAt(index)== 'a')){ //q4
                  index += 1;
               }
            }
        }else{
           accepted_state = false;
        }
        return accepted_state;
    }
}
