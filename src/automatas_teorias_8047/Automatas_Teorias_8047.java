/*
# You can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, version 2.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatas_teorias_8047;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
/**
 *
 * @author M4RS
 */
public class Automatas_Teorias_8047 {

   private String input;

   public Automatas_Teorias_8047(String input) {

        this.input = input;
   }
   
   //this method simulates the behavior of the estring on the automata

    public boolean validation() {
        int size = input.length();
        int index = 0;
        boolean accepted_state = true;

        //starts in q0
        if (index < size && input.charAt(index) == 'b')//qo 
        {
            //move q4
            index += 1;
            if (index < size && input.charAt(index) == 'a')//q4 
            {
                //move q2
                index += 1;
            } else if (index < size && input.charAt(index) == 'b')//q4 
            {
                //move q3
                index += 1;

                while (index < size && input.charAt(index) == 'b') //q3
                {

                    //Loop in q3
                    index += 1;
                }
                if (index < size && input.charAt(index) == 'a') //q3
                {
                    //move q1
                    index += 1;
                    if (index < size && input.charAt(index) == 'a') //q1
                    {
                        //move q2
                        index += 1;
                    } else {
                        //wrong symbol
                        accepted_state = false;
                    }
                } else {
                    //wrong symbol
                    accepted_state = false;
                }
            } else {
                //wrong symbol
                accepted_state = false;
            }
            
            //q2
            while (accepted_state && index< size && (input.charAt(index)== 'a' || input.charAt(index)=='b')) 
            {                
                //loop in q2
                index +=1;
                
            }
            if (index != size) 
            {
             accepted_state=false;
             
            } 
        } else {
            //wrong symbol!
            accepted_state = false;
        }
        return accepted_state;
    }
    
   /**
    * @param args the command line arguments
    * Cambiar el nombre de objeto Automatas_Teorias_8047_# para cada ejercicio - Descomentar el ejercicio
    */
   public static void main(String[] args) {
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));//buffer to read from string
      BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));//bufer write from string

        try {
            String input = br.readLine();//read input string of the automata
            //Automatas_Teorias_8047_1 demo = new Automatas_Teorias_8047_1(input); //create abvstaract machine and gives parameters
            //Automatas_Teorias_8047 demo = new Automatas_Teorias_8047(input); //create abvstaract machine and gives parameters
            //Automatas_Teorias_8047_3 demo = new Automatas_Teorias_8047_3(input); //create abvstaract machine and gives parameters
            //Automatas_Teorias_8047_4 demo = new Automatas_Teorias_8047_4(input); //create abvstaract machine and gives parameters
            bw.write(input);
            bw.write(input.length());
            if (demo.validation()) { //if input es accepted
                bw.write(" cadena aceptada\n");
            } else {//if input not accepted
                bw.write(" cadena rechazada\n");
            }
            bw.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
   }
   
}
